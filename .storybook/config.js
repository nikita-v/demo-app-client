import { withTests } from '@storybook/addon-jest'
import { addDecorator, configure } from '@storybook/react'

import results from '../.jest-test-results.json'
import '../src/styles.scss'

addDecorator(
  withTests({
    results,
    filesExt: '((\\.specs?)|(\\.tests?))?(\\.tsx)?$',
  }),
)

const req = require.context('../src/components', true, /.stories.tsx$/)
configure(() => req.keys().forEach(req), module)
