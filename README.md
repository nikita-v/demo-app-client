## Tools/Features

- **Create React App + TypeScript**
- **Apollo GraphGL** (local state, cache, union types)
- **Testing** (Jest, Enzyme, Apollo test utils)
- **Storybook** (Jest addon)
- **i18n** (i18next)
- **Routing** (react-router)
- **Forms** (formik)
- **Styles** (sass, bulma, bulma-steps plugin)
- **Lint** (tslint, prettier)
