import React from 'react'
import { ApolloProvider } from 'react-apollo'
import { render } from 'react-dom'
import { BrowserRouter } from 'react-router-dom'

import client from './client'
import { App } from './components'
import './i18n'
import { register as serviceWorker } from './serviceWorker'
import './styles.scss'

render(
  <BrowserRouter>
    <ApolloProvider client={client}>
      <App />
    </ApolloProvider>
  </BrowserRouter>,
  document.getElementById('root'),
)

serviceWorker()
