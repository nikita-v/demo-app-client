import {
  InMemoryCache,
  IntrospectionFragmentMatcher,
} from 'apollo-cache-inmemory'

export const fragmentMatcher = new IntrospectionFragmentMatcher({
  introspectionQueryResultData: {
    __schema: {
      types: [
        {
          kind: 'UNION',
          name: 'UserRole',
          possibleTypes: [{ name: 'CompanyRole' }, { name: 'CourseRole' }],
        },
      ],
    },
  },
})

export default new InMemoryCache({
  fragmentMatcher,
})
