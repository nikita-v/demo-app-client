import { withClientState } from 'apollo-link-state'

import { HEADER_MENU_IS_OPENED } from '../queries'
import cache from './cache'

export default withClientState({
  cache,
  defaults: {
    headerMenuIsOpened: false,
  },
  resolvers: {
    Mutation: {
      toggleHeaderMenu: () => {
        const { headerMenuIsOpened = false } =
          cache.readQuery({ query: HEADER_MENU_IS_OPENED }) || {}

        cache.writeData({ data: { headerMenuIsOpened: !headerMenuIsOpened } })
      },
    },
  },
})
