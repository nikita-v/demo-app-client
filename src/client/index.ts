import { ApolloClient } from 'apollo-client'
import { ApolloLink, from } from 'apollo-link'
import { HttpLink } from 'apollo-link-http'

import cache from './cache'
import state from './state'

const httpLink = new HttpLink({ uri: process.env.REACT_APP_GRAPHQL_URI })

const authLink = new ApolloLink((operation, forward: any) => {
  operation.setContext({
    headers: {
      token: localStorage.getItem('token'),
    },
  })

  return forward(operation)
})

export default new ApolloClient({
  cache,
  link: from([state, authLink, httpLink]),
})
