interface IRule {
  id: string
  name: string
}

interface IRole {
  id: string
  title: string
  companyId?: string
  courseId?: string
  rules: IRule[]
}

interface IUser {
  roles: IRole[]
}

interface IArgs {
  user?: IUser
  companyId?: string
  courseId?: string
  rules: string[]
}

export const hasAccess = ({
  user: { roles } = { roles: [] },
  companyId,
  // courseId,
  rules,
}: IArgs): boolean => {
  if (companyId) {
    return roles.some(
      role =>
        role.companyId === companyId &&
        role.rules.some(
          ({ name }) => name === 'COMPANY_OWNER' || rules.includes(name),
        ),
    )
  }

  return false
}
