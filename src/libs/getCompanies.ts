interface ICompany {
  id: string
  name: string
  title: string
}

interface ICourseWithCompanies {
  company: ICompany
}

export const getCompanies = (courses: ICourseWithCompanies[]): ICompany[] =>
  courses.reduce(
    (all: ICompany[], { company }) =>
      all.findIndex(({ id }) => company.id === id) === -1
        ? all.concat(company)
        : all,
    [],
  )
