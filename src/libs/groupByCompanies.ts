import { getCompanies } from './'

interface ICourse {
  id: string
  name: string
  title: string
  description: string
}

interface ICompany {
  id: string
  name: string
  title: string
}

interface ICourseWithCompanies extends ICourse {
  company: ICompany
}

interface IGroupedByCompany {
  company: ICompany
  courses: ICourse[]
}

interface IArgs {
  maxItems?: number
  courses: ICourseWithCompanies[]
  hiddenCompanies?: string[]
}

export const groupByCompanies = ({
  courses,
  hiddenCompanies = [],
}: IArgs): IGroupedByCompany[] =>
  getCompanies(courses)
    .filter(({ id }) => !hiddenCompanies.includes(id))
    .map((company, _: any) => ({
      company,
      courses: courses.filter(({ company: { id } }) => id === company.id),
    }))
