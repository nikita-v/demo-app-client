import i18n from 'i18next'
import { initReactI18next } from 'react-i18next'

export default i18n.use(initReactI18next).init({
  fallbackLng: 'en',
  lng: 'en',
  resources: {
    en: {
      translation: {
        // Courses
        coursesCreateCourseCardTitle: 'Create course',
        // Header
        headerMenuLinkToCourses: 'Courses',
        headerMenuLinkToFeed: 'Feed',
        headerMenuLoginButton: 'Log In',
        headerMenuLogoutButton: 'Log Out',
        // Login
        loginFormFieldEmailLabel: 'Email',
        loginFormFieldEmailPlaceholder: 'e.g coach@mail.com',
        loginFormFieldPasswordLabel: 'Password',
        loginFormFieldPasswordPlaceholder: '••••••',
        loginFormSubmitButton: 'Log In',
      },
    },
  },
})
