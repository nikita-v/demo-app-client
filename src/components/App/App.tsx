import React from 'react'
import { Query } from 'react-apollo'
import { Redirect, Route, Switch, withRouter } from 'react-router-dom'

import { Company, Courses, Feed, Header, Loading, Login, New } from '../'
import { USER } from '../../queries'

export const App = withRouter(({ location: { pathname } }) => (
  <Query query={USER}>
    {({ loading, error, data: { user } = { user: null } }) => {
      if (loading) {
        return <Loading />
      }

      return (
        <div className="container">
          {pathname !== '/login' && <Header />}

          <Switch>
            <Route path="/login" component={Login} />

            <Route path="/@:companyName" component={Company} />

            <Route
              render={({ history: { location } }) => {
                if (error || !user) {
                  return (
                    <Redirect
                      to={{ pathname: '/login', state: { from: location } }}
                    />
                  )
                }

                return (
                  <Switch>
                    <Route exact path="/" component={Courses} />

                    <Route path="/feed" component={Feed} />

                    <Route path="/new" component={New} />
                  </Switch>
                )
              }}
            />
          </Switch>
        </div>
      )
    }}
  </Query>
))
