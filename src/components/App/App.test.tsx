import { mount } from 'enzyme'
import React from 'react'

import { Company, Courses, Header, Login } from '../'
import { MockedProvider, userQueryMock } from '../../mocks'
import { App } from './App'

it('redirects to login if not authorized user tries to open a private page', async () => {
  const component = mount(
    <MockedProvider
      mocks={[{ ...userQueryMock, result: { data: { user: null } } }]}
    >
      <App />
    </MockedProvider>,
  )

  await new Promise(r => setTimeout(r, 0))
  component.update()

  expect(component.find(Login).exists()).toBeTruthy()
  expect(component.find(Courses).exists()).toBeFalsy()
})

it('redirects to login on error at private page too', async () => {
  const component = mount(
    <MockedProvider mocks={[{ ...userQueryMock, error: new Error('NotAuth') }]}>
      <App />
    </MockedProvider>,
  )

  await new Promise(r => setTimeout(r, 0))
  component.update()

  expect(component.find(Login).exists()).toBeTruthy()
})

it('shows a public page for any users', async () => {
  const component = mount(
    <MockedProvider initialIndex={3}>
      <App />
    </MockedProvider>,
  )

  await new Promise(r => setTimeout(r, 0))
  component.update()

  expect(component.find(Company).exists()).toBeTruthy()
})

it('shows header if user not on login page', async () => {
  const atCompanyPage = mount(
    <MockedProvider initialIndex={3}>
      <App />
    </MockedProvider>,
  )

  await new Promise(r => setTimeout(r, 0))
  atCompanyPage.update()

  expect(atCompanyPage.find(Header).exists()).toBeTruthy()

  const atLoginPage = mount(
    <MockedProvider
      initialIndex={0}
      mocks={[{ ...userQueryMock, result: { data: { user: null } } }]}
    >
      <App />
    </MockedProvider>,
  )

  await new Promise(r => setTimeout(r, 0))
  atLoginPage.update()

  expect(atLoginPage.find(Header).exists()).toBeFalsy()
})
