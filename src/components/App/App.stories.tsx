import { storiesOf } from '@storybook/react'
import React from 'react'
import { ApolloProvider } from 'react-apollo'
import { BrowserRouter } from 'react-router-dom'

import client from '../../client'
import { App } from './App'

storiesOf('App', module)
  .addParameters({ jest: ['App'] })
  .addDecorator(storyFn => (
    <BrowserRouter>
      <ApolloProvider client={client}>{storyFn()}</ApolloProvider>
    </BrowserRouter>
  ))
  .add('App', () => <App />)
