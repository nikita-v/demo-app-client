import classNames from 'classnames'
import React from 'react'
import { Link } from 'react-router-dom'

interface IProps {
  to?: string
  children: React.ReactNode
  type?: string
  isMedium?: boolean
  isPrimary?: boolean
  isFullwidth?: boolean
  isLoading?: boolean
  disabled?: boolean
  onClick?: (...args: any) => any
}

export const Button = ({
  to,
  children,
  type = 'button',
  isMedium,
  isPrimary,
  isFullwidth,
  isLoading,
  disabled,
  onClick,
}: IProps) => {
  const className = classNames(
    'button',
    { 'is-medium': isMedium },
    { 'is-primary': isPrimary },
    { 'is-fullwidth': isFullwidth },
    { 'is-loading': isLoading },
  )

  if (to) {
    return (
      <Link to={to} className={className}>
        {children}
      </Link>
    )
  }

  return (
    <button
      type={type}
      className={className}
      disabled={disabled}
      onClick={onClick}
    >
      {children}
    </button>
  )
}
