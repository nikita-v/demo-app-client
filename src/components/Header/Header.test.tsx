import { mount } from 'enzyme'
import React from 'react'
import { NavLink } from 'react-router-dom'

import { Button } from '../'
import {
  headerMenuIsOpenedQueryMock,
  MockedProvider,
  userQueryMock,
} from '../../mocks'
import { TOGGLE_HEADER_MENU } from './Burger'
import { Header } from './Header'
import { Menu } from './Menu'

export const toggleMenuMock = {
  request: { query: TOGGLE_HEADER_MENU },
  result: {
    data: {
      toggleHeaderMenu: null,
    },
  },
}

it('toggles header menu', async () => {
  const component = mount(
    <MockedProvider
      mocks={[userQueryMock, headerMenuIsOpenedQueryMock, toggleMenuMock]}
    >
      <Header />
    </MockedProvider>,
  )

  expect(
    component
      .find(Menu)
      .find('.navbar-menu')
      .prop('className'),
  ).toBe('navbar-menu')

  await new Promise(r => setTimeout(r, 0))
  component.update()

  expect(
    component
      .find(Menu)
      .find('.navbar-menu')
      .prop('className'),
  ).toBe('navbar-menu is-active')
})

it('shows links to feed and courses if user is auth', async () => {
  const component = mount(
    <MockedProvider
      mocks={[userQueryMock, headerMenuIsOpenedQueryMock, toggleMenuMock]}
    >
      <Header />
    </MockedProvider>,
  )

  expect(
    component
      .find(Menu)
      .find('.navbar-start')
      .find(NavLink)
      .exists(),
  ).toBeFalsy()

  await new Promise(r => setTimeout(r, 0))
  component.update()

  const links = component
    .find(Menu)
    .find('.navbar-start')
    .find(NavLink)

  expect(links.first().prop('to')).toBe('/')
  expect(links.at(1).prop('to')).toBe('/feed')
})

it('shows login button if user is not auth', async () => {
  const component = mount(
    <MockedProvider
      mocks={[
        { ...userQueryMock, result: { data: { user: null } } },
        headerMenuIsOpenedQueryMock,
        toggleMenuMock,
      ]}
    >
      <Header />
    </MockedProvider>,
  )

  await new Promise(r => setTimeout(r, 0))
  component.update()

  expect(
    component
      .find(Menu)
      .find('.navbar-end')
      .find(Button)
      .text(),
  ).toBe('Log In')
})

it('contains logout button if user is auth', async () => {
  const component = mount(
    <MockedProvider>
      <Header />
    </MockedProvider>,
  )

  await new Promise(r => setTimeout(r, 0))
  component.update()

  expect(
    component
      .find(Menu)
      .find('.navbar-end')
      .find(Button)
      .text(),
  ).toBe('Log Out')
})
