import classNames from 'classnames'
import React from 'react'
import { Query } from 'react-apollo'
import { useTranslation } from 'react-i18next'
import { NavLink } from 'react-router-dom'

import { Button } from '../'
import { HEADER_MENU_IS_OPENED, USER } from '../../queries'

export const Menu = () => {
  const { t } = useTranslation()

  return (
    <Query query={HEADER_MENU_IS_OPENED}>
      {({ data: { headerMenuIsOpened } = { headerMenuIsOpened: false } }) => (
        <div
          className={classNames('navbar-menu', {
            'is-active': headerMenuIsOpened,
          })}
        >
          <Query query={USER}>
            {({ data: { user } = { user: null }, client: { resetStore } }) => {
              if (user) {
                return (
                  <>
                    <div className="navbar-start">
                      <NavLink
                        to="/"
                        exact
                        className="navbar-item"
                        activeClassName="is-active"
                      >
                        {t('headerMenuLinkToCourses')}
                      </NavLink>

                      <NavLink
                        to="/feed"
                        className="navbar-item"
                        activeClassName="is-active"
                      >
                        {t('headerMenuLinkToFeed')}
                      </NavLink>
                    </div>

                    <div className="navbar-end">
                      <div className="navbar-item">
                        <Button
                          onClick={() =>
                            resetStore() && localStorage.removeItem('token')
                          }
                        >
                          {t('headerMenuLogoutButton')}
                        </Button>
                      </div>
                    </div>
                  </>
                )
              }

              return (
                <div className="navbar-end">
                  <div className="navbar-item">
                    <Button to="/login">{t('headerMenuLoginButton')}</Button>
                  </div>
                </div>
              )
            }}
          </Query>
        </div>
      )}
    </Query>
  )
}
