import { storiesOf } from '@storybook/react'
import React from 'react'

import {
  headerMenuIsOpenedQueryMock,
  MockedProvider,
  userQueryMock,
} from '../../mocks'
import { TOGGLE_HEADER_MENU } from './Burger'
import { Header } from './Header'

export const toggleMenuMock = {
  request: { query: TOGGLE_HEADER_MENU },
  result: {
    data: {
      toggleHeaderMenu: null,
    },
  },
}

storiesOf('Header', module)
  .addParameters({ jest: ['Header'] })
  .add('User is auth', () => (
    <MockedProvider
      mocks={[userQueryMock, headerMenuIsOpenedQueryMock, toggleMenuMock]}
    >
      <Header />
    </MockedProvider>
  ))
  .add('User is unauth', () => (
    <MockedProvider
      mocks={[
        { ...userQueryMock, result: { data: { user: null } } },
        headerMenuIsOpenedQueryMock,
        toggleMenuMock,
      ]}
    >
      <Header />
    </MockedProvider>
  ))
