import gql from 'graphql-tag'
import React from 'react'
import { Mutation } from 'react-apollo'

export const TOGGLE_HEADER_MENU = gql`
  mutation toggleHeaderMenu {
    toggleHeaderMenu @client
  }
`

export const Burger = () => (
  <Mutation mutation={TOGGLE_HEADER_MENU}>
    {toggleHeaderMenu => (
      <a
        role="button"
        className="navbar-burger"
        aria-label="menu"
        aria-expanded="false"
        onClick={toggleHeaderMenu as () => void}
      >
        <span aria-hidden="true" />
        <span aria-hidden="true" />
        <span aria-hidden="true" />
      </a>
    )}
  </Mutation>
)
