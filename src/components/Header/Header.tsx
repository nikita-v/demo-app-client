import React from 'react'

import { Burger } from './Burger'
import { Menu } from './Menu'

export const Header = () => (
  <nav
    className="navbar is-fixed-top"
    role="navigation"
    aria-label="main navigation"
  >
    <div className="container">
      <div className="navbar-brand">
        <div className="navbar-item">
          <img
            src="https://bulma.io/images/bulma-logo.png"
            alt="Bulma: Free, open source, & modern CSS framework based on Flexbox"
            width="112"
            height="28"
          />
        </div>

        <Burger />
      </div>

      <Menu />
    </div>
  </nav>
)
