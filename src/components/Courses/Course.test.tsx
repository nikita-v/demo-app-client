import { mount } from 'enzyme'
import React from 'react'
import { Link } from 'react-router-dom'

import { MockedProvider } from '../../mocks'
import { Course } from './Course'

const component = mount(
  <MockedProvider>
    <Course
      companyName="company"
      name="course"
      title="Title"
      description="Description"
    />
  </MockedProvider>,
)

it('is link to a course', () => {
  expect(component.find(Link).prop('to')).toBe('/@company/course')
})

it('contains title and description', () => {
  expect(component.find('h4').text()).toBe('Title')

  expect(component.find('p').text()).toBe('Description')
})
