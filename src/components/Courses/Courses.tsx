import React from 'react'
import { Query } from 'react-apollo'

import { Loading } from '../'
import { groupByCompanies } from '../../libs'
import { COURSES } from '../../queries'
import { Company } from './Company'
import { Course } from './Course'

export const Courses = () => (
  <Query query={COURSES}>
    {({ loading, error, data }) => {
      if (loading) {
        return <Loading />
      }

      if (error) {
        return JSON.stringify(error)
      }

      return groupByCompanies(data).map(({ company, courses }) => (
        <Company key={company.id} companyId={company.id} title={company.title}>
          {courses.map(course => (
            <Course key={course.id} companyName={company.name} {...course} />
          ))}
        </Company>
      ))
    }}
  </Query>
)
