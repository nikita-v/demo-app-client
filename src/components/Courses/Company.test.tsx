import { mount } from 'enzyme'
import React from 'react'

import { MockedProvider } from '../../mocks'
import { Company, CreateCourseCard } from './Company'
import { Course } from './Course'

const render = (companyId: string = 'c1') =>
  mount(
    <MockedProvider>
      <Company companyId={companyId} title="test-company">
        <Course
          companyName="test-company"
          name="test-course-1"
          title="Test Course 1"
          description="..."
        />
      </Company>
    </MockedProvider>,
  )

it('contains title and list of courses', async () => {
  const wrapper = render()

  await new Promise(r => setTimeout(r, 0))
  wrapper.update()

  expect(wrapper.find('h3').text()).toBe('test-company')

  expect(wrapper.find(Course)).toHaveLength(1)
})

it('shows create course card if user has an access', async () => {
  let wrapper = render()

  await new Promise(r => setTimeout(r, 0))
  wrapper.update()

  expect(wrapper.find(CreateCourseCard).exists()).toBeTruthy()

  wrapper = render('cantCreateCoursesCompanyId')

  await new Promise(r => setTimeout(r, 0))
  wrapper.update()

  expect(wrapper.find(CreateCourseCard).exists()).toBeFalsy()
})
