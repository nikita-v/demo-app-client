import React from 'react'
import { Link } from 'react-router-dom'

interface IProps {
  companyName: string
  name: string
  title: string
  description: string
}

export const Course = ({ companyName, name, title, description }: IProps) => (
  <Link className="column is-one-third" to={`/@${companyName}/${name}`}>
    <div className="box card">
      <h4 className="title is-4">{title}</h4>

      <p>{description}</p>
    </div>
  </Link>
)
