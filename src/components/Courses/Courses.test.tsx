import { mount } from 'enzyme'
import React from 'react'

import { MockedProvider } from '../../mocks'
import { Company } from './Company'
import { Course } from './Course'
import { Courses } from './Courses'

it('shows courses grouped by companies', async () => {
  const component = mount(
    <MockedProvider>
      <Courses />
    </MockedProvider>,
  )

  expect(component.find(Course).exists()).toBeFalsy()

  await new Promise(r => setTimeout(r, 0))
  component.update()

  const companies = component.find(Company)

  expect(companies.first().find(Course)).toHaveLength(2)
  expect(companies.at(1).find(Course)).toHaveLength(1)
})
