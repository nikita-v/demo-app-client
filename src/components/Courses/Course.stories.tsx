import { storiesOf } from '@storybook/react'
import React from 'react'

import { MockedProvider } from '../../mocks'
import { Course } from './Course'

storiesOf('Courses', module)
  .addParameters({ jest: ['Course'] })
  .addDecorator(storyFn => <MockedProvider>{storyFn()}</MockedProvider>)
  .add('Course', () => (
    <Course
      companyName="company"
      name="course"
      title="Course Title"
      description="Some description"
    />
  ))
