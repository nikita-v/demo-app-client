import { storiesOf } from '@storybook/react'
import React from 'react'

import { MockedProvider } from '../../mocks'
import { Courses } from './Courses'

storiesOf('Courses', module)
  .addParameters({ jest: ['Courses'] })
  .addDecorator(storyFn => <MockedProvider>{storyFn()}</MockedProvider>)
  .add('Default', () => <Courses />)
