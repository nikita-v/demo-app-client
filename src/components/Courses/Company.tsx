import React, { ReactNode } from 'react'
import { Query } from 'react-apollo'
import { Translation } from 'react-i18next'
import { Link } from 'react-router-dom'

import { hasAccess } from '../../libs'
import { USER } from '../../queries'

interface IProps {
  companyId: string
  title: string
  children: ReactNode
}

export const CreateCourseCard = () => (
  <Link className="column is-one-third" to="/new">
    <div className="box card is-shadowless has-background-light">
      <h4 className="title is-4">
        <Translation>{t => t('coursesCreateCourseCardTitle')}</Translation>
      </h4>
    </div>
  </Link>
)

export const Company = ({ companyId, title, children }: IProps) => (
  <section className="section">
    <h3 className="title is-3">{title}</h3>

    <div className="columns is-multiline">
      {children}

      <Query query={USER}>
        {({ data: { user } = { user: null } }) =>
          hasAccess({ companyId, user, rules: ['COMPANY_COURSE_CREATE'] }) && (
            <CreateCourseCard />
          )
        }
      </Query>
    </div>
  </section>
)
