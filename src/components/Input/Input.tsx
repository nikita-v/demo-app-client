import { FieldProps } from 'formik'
import React from 'react'

interface IProps extends FieldProps {
  type?: string
  label?: string
  placeholder: string
}

export const Input = ({
  field,
  form: { touched, errors },
  type = 'text',
  label,
  placeholder,
}: IProps) => (
  <div className="field">
    {label && <label className="label">{label}</label>}

    <div className="control">
      <input
        type={type}
        className={`input ${
          touched[field.name] && errors[field.name] ? ' is-danger' : ''
        }`}
        placeholder={placeholder}
        {...field}
      />
    </div>

    {touched[field.name] && errors[field.name] && (
      <p className="help is-danger">{JSON.stringify(errors[field.name])}</p>
    )}
  </div>
)
