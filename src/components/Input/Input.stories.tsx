import { storiesOf } from '@storybook/react'
import { Field, Form, Formik } from 'formik'
import React from 'react'

import { Input } from './Input'

storiesOf('Input', module)
  .addDecorator(storyFn => <div className="section">{storyFn()}</div>)
  .add('Default', () => (
    <Formik
      onSubmit={() => true}
      initialValues={{ loginEmail: '' }}
      render={() => (
        <Form>
          <Field
            type="email"
            name="loginEmail"
            placeholder="e.g coach@mail.com"
            component={Input}
          />
        </Form>
      )}
    />
  ))
  .add('With Error', () => (
    <Formik
      onSubmit={() => true}
      initialValues={{ loginEmail: 'wrng@email' }}
      render={() => (
        <Form>
          <Field
            type="email"
            name="loginEmail"
            placeholder="e.g coach@mail.com"
            component={Input}
            form={{
              errors: { loginEmail: 'Wrong Email' },
              touched: { loginEmail: true },
            }}
          />
        </Form>
      )}
    />
  ))
