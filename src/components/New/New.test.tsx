import { mount } from 'enzyme'
import React from 'react'

import { MockedProvider } from '../../mocks'
import { Course } from './Course'
import { New } from './New'
import { Steps } from './Steps'

it('contains stepper', async () => {
  const component = mount(
    <MockedProvider>
      <New />
    </MockedProvider>,
  )

  await new Promise(r => setTimeout(r, 0))
  component.update()

  expect(component.find(Steps).exists()).toBeTruthy()
})

it('redirects to course info page initially', async () => {
  const component = mount(
    <MockedProvider initialIndex={4}>
      <New />
    </MockedProvider>,
  )

  await new Promise(r => setTimeout(r, 0))
  component.update()

  expect(component.find(Course).exists()).toBeTruthy()
})
