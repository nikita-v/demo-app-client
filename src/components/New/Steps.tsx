import cns from 'classnames'
import React from 'react'
import { Link, withRouter } from 'react-router-dom'

interface ISegment {
  to: string
  isActive?: boolean
  title?: string
}

const Segment = ({ to, isActive, title }: ISegment) => (
  <li className={cns('steps-segment', { 'is-active has-gaps': isActive })}>
    <Link to={`/new${to}`} className="has-text-dark has-text-weight-light">
      <span className={cns('steps-marker', { 'is-hollow': isActive })}>
        <span />
      </span>

      {title && <div className="steps-content is-divider-content">{title}</div>}
    </Link>
  </li>
)

export const Steps = withRouter(({ location: { pathname } }) => (
  <ul className="steps">
    <Segment
      to="/course"
      isActive={pathname === '/new/course'}
      title="Course"
    />

    <Segment
      to="/lessons"
      isActive={pathname === '/new/lessons'}
      title="Lessons"
    />

    <Segment to="/users" isActive={pathname === '/new/users'} title="Users" />

    <Segment to="/final" isActive={pathname === '/new/final'} />
  </ul>
))
