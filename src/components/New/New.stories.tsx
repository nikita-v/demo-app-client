import { storiesOf } from '@storybook/react'
import React from 'react'

import { MockedProvider } from '../../mocks'
import { New } from './New'

storiesOf('New', module)
  .addParameters({ jest: ['New'] })
  .addDecorator(storyFn => <MockedProvider>{storyFn()}</MockedProvider>)
  .add('Default', () => <New />)
