import { storiesOf } from '@storybook/react'
import React from 'react'

import { MockedProvider } from '../../mocks'
import { Steps } from './Steps'

storiesOf('New/Steps', module)
  .addDecorator(storyFn => <MockedProvider>{storyFn()}</MockedProvider>)
  .add('Default', () => <Steps />)
