import React from 'react'
import { Redirect, Route, Switch, withRouter } from 'react-router-dom'

import { Course } from './Course'
import { Final } from './Final'
import { Lessons } from './Lessons'
import { Steps } from './Steps'
import { Users } from './Users'

export const New = withRouter(({ match: { url } }) => (
  <>
    <Steps />

    <Switch>
      <Route path={`${url}/course`} component={Course} />

      <Route path={`${url}/lessons`} component={Lessons} />

      <Route path={`${url}/users`} component={Users} />

      <Route path={`${url}/final`} component={Final} />

      <Redirect to={`${url}/course`} />
    </Switch>
  </>
))
