import { Formik } from 'formik'
import gql from 'graphql-tag'
import React from 'react'
import { Mutation, Query } from 'react-apollo'
import { Redirect, withRouter } from 'react-router-dom'

import { USER } from '../../queries'
import { LoginForm } from './Form'

export const LOGIN = gql`
  mutation login($loginEmail: String!, $loginPassword: String!) {
    login(loginEmail: $loginEmail, loginPassword: $loginPassword) {
      token
      user {
        id
        profile {
          name
        }
        roles {
          id
          title
          rules {
            id
            name
          }
          ... on CourseRole {
            courseId
          }
          ... on CompanyRole {
            companyId
          }
        }
      }
    }
  }
`

export const Login = withRouter(({ location }) => (
  <Query query={USER}>
    {({ loading, error, data: { user } = { user: null } }) => {
      if (!loading && !error && user) {
        return <Redirect to={location.state ? location.state.from : '/'} />
      }

      return (
        <Mutation
          mutation={LOGIN}
          update={(cache, { data: { login } }) => {
            cache.writeQuery({ data: login, query: USER })

            localStorage.setItem('token', login.token)
          }}
          onCompleted={() => window.location.reload()}
        >
          {login => (
            <Formik
              initialValues={{ loginEmail: '', loginPassword: '' }}
              onSubmit={async (variables, { setSubmitting, setErrors }) => {
                try {
                  await login({ variables })
                } catch ({ graphQLErrors }) {
                  if (graphQLErrors) {
                    graphQLErrors.forEach(({ data }: any) => setErrors(data))
                  }
                } finally {
                  setSubmitting(false)
                }
              }}
              render={LoginForm}
            />
          )}
        </Mutation>
      )
    }}
  </Query>
))
