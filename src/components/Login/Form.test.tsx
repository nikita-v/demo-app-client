import { mount } from 'enzyme'
import { Field, Formik } from 'formik'
import React from 'react'

import { MockedProvider } from '../../mocks'
import { LoginForm } from './Form'

it('contains email and password fields', () => {
  const component = mount(
    <MockedProvider>
      <Formik
        initialValues={{ loginEmail: '', loginPassword: '' }}
        component={LoginForm}
        onSubmit={() => true}
      />
    </MockedProvider>,
  )

  expect(
    component
      .find(Field)
      .first()
      .prop('name'),
  ).toBe('loginEmail')

  expect(
    component
      .find(Field)
      .at(1)
      .prop('name'),
  ).toBe('loginPassword')
})

it('contains submit button', () => {
  const component = mount(
    <MockedProvider>
      <Formik
        initialValues={{ loginEmail: '', loginPassword: '' }}
        component={LoginForm}
        onSubmit={() => true}
      />
    </MockedProvider>,
  )

  expect(component.find('button').prop('type')).toBe('submit')
})
