import { mount } from 'enzyme'
import React from 'react'

import { MockedProvider } from '../../mocks'
import { Login, LOGIN } from './Login'

const loginMock = {
  request: {
    query: LOGIN,
    variables: { loginEmail: 'test@user.co', loginPassword: '123456' },
  },
  result: {
    data: {
      token: 'jwt',
      user: { id: '1', profile: { name: 'Test User' } },
    },
  },
}

it('should redirect to prev page or "/", if user has been authorized already', async () => {
  const component = mount(
    <MockedProvider initialIndex={0}>
      <Login location={{ state: { from: { pathname: '/feed' } } }} />
    </MockedProvider>,
  )

  await new Promise(r => setTimeout(r, 0))
  component.update()

  expect(component.html()).toBeNull()
})

it('renders form, if user is not auth', async () => {
  const component = mount(
    <MockedProvider mocks={[loginMock]} initialIndex={0}>
      <Login location={{}} />
    </MockedProvider>,
  )

  await new Promise(r => setTimeout(r, 0))
  component.update()

  expect(component.find('form').exists()).toBeTruthy()
})
