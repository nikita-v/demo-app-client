import { Field, Form, FormikProps } from 'formik'
import React from 'react'
import { Translation } from 'react-i18next'

import { Button, Input } from '../'

export const LoginForm = ({
  isSubmitting,
  isValid,
}: FormikProps<{ loginEmail: string; loginPassword: string }>) => {
  return (
    <Translation>
      {t => (
        <Form>
          <Field
            type="email"
            name="loginEmail"
            label={t('loginFormFieldEmailLabel')}
            placeholder={t('loginFormFieldEmailPlaceholder')}
            component={Input}
          />

          <Field
            type="password"
            name="loginPassword"
            label={t('loginFormFieldPasswordLabel')}
            placeholder={t('loginFormFieldPasswordPlaceholder')}
            component={Input}
          />

          <Button type="submit" disabled={isSubmitting || !isValid} isPrimary>
            {t('loginFormSubmitButton')}
          </Button>
        </Form>
      )}
    </Translation>
  )
}
