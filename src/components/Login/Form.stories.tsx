import { storiesOf } from '@storybook/react'
import { Formik } from 'formik'
import React from 'react'

import { LoginForm } from './Form'

storiesOf('Login/Form', module)
  .addParameters({ jest: ['Form'] })
  .addDecorator(storyFn => <div className="section">{storyFn()}</div>)
  .add('Default', () => (
    <Formik
      initialValues={{ loginEmail: '', loginPassword: '' }}
      component={LoginForm}
      onSubmit={() => true}
    />
  ))
