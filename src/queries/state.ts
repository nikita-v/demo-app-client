import gql from 'graphql-tag'

export const HEADER_MENU_IS_OPENED = gql`
  query {
    headerMenuIsOpened @client
  }
`
