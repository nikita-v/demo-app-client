import gql from 'graphql-tag'

export const USER = gql`
  query {
    user {
      id
      profile {
        name
      }
      roles {
        id
        title
        rules {
          id
          name
        }
        ... on CourseRole {
          courseId
        }
        ... on CompanyRole {
          companyId
        }
      }
    }
  }
`
