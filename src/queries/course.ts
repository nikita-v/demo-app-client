import gql from 'graphql-tag'

export const COURSES = gql`
  query {
    courses {
      id
      name
      title
      description
      company {
        id
        name
        title
      }
    }
  }
`
