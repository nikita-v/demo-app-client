import { InMemoryCache } from 'apollo-cache-inmemory'
import React, { ReactNode } from 'react'
import { MockedProvider as ApolloMockedProvider } from 'react-apollo/test-utils'
import { MemoryRouter } from 'react-router-dom'

import { fragmentMatcher } from '../client/cache'
import '../i18n'
import * as queries from './queries'

interface IMock {
  request: { query: any; variables?: any }
  result: { data: any }
  error?: Error
}
interface IProps {
  children: ReactNode
  mocks?: IMock[]
  initialIndex?: number
}

export const MockedProvider = ({
  children,
  mocks,
  initialIndex = 1,
}: IProps) => {
  const cache = new InMemoryCache({ fragmentMatcher })

  return (
    <MemoryRouter
      initialEntries={['/login', '/', '/feed', '/@:companyName', '/new']}
      initialIndex={initialIndex}
    >
      <ApolloMockedProvider
        mocks={mocks || Object.values(queries)}
        cache={cache}
        defaultOptions={{ query: { fetchPolicy: 'no-cache' } }}
      >
        {children}
      </ApolloMockedProvider>
    </MemoryRouter>
  )
}
