import { COURSES } from '../../queries/'

export const coursesQueryMock = {
  request: { query: COURSES },
  result: {
    data: {
      courses: [
        {
          __typename: 'Course',
          company: {
            __typename: 'Company',
            id: 'c1',
            name: 'test-company',
            title: 'Test Company',
          },
          description: 'Test course description',
          id: '1',
          name: 'test-course',
          title: 'Test Course',
        },
        {
          __typename: 'Course',
          company: {
            __typename: 'Company',
            id: 'c2',
            name: 'test-company-2',
            title: 'Test Company 2',
          },
          description: 'Test course description',
          id: '2',
          name: 'test-course-2',
          title: 'Test Course 2',
        },
        {
          __typename: 'Course',
          company: {
            __typename: 'Company',
            id: 'c1',
            name: 'test-company',
            title: 'Test Company',
          },
          description: 'Test course description',
          id: '3',
          name: 'test-course-3',
          title: 'Test Course 3',
        },
      ],
    },
  },
}
