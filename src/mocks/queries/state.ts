import { HEADER_MENU_IS_OPENED } from '../../queries'

export const headerMenuIsOpenedQueryMock = {
  request: { query: HEADER_MENU_IS_OPENED },
  result: { data: { headerMenuIsOpened: true } },
}
