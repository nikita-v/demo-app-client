import { USER } from '../../queries/'

export const userQueryMock = {
  request: { query: USER },
  result: {
    data: {
      user: {
        __typename: 'User',
        id: '1',
        profile: { __typename: 'UserProfile', name: 'Test User' },
        roles: [
          {
            __typename: 'CompanyRole',
            companyId: 'c1',
            id: 'role1',
            rules: [
              {
                __typename: 'Rule',
                id: 'rule1',
                name: 'COMPANY_COURSE_CREATE',
              },
            ],
            title: 'Company 1 Owners',
          },
        ],
      },
    },
  },
}
